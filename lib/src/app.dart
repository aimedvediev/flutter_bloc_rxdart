import 'package:flutter/material.dart';
import 'package:flutter_bloc_rxdart/src/blocs/bloc.dart';

import 'package:flutter_bloc_rxdart/src/screens/login_screen.dart';
import 'package:provider/provider.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<Bloc>(
      create: (context) => Bloc(),
      dispose: (context, bloc) => bloc.dispose(),
      child: MaterialApp(
        title: 'Log me in',
        home: Scaffold(
          body: LoginScreen(),
        ),
      ),
    );
  }
}
